# elasticsearch-tools

A collection of scripts for interacting with Elasticsearch/Opensearch clusters on AWS.

Create a `.env` file in the root of the repo with contents like:

```
# The https:// at the beginning and / at the end are important
SUPERCLUSTER="https://vpc-supercluster-asdf1234.us-east-1.es.amazonaws.com/"
```

Then invoke with:

```
python3 src/main.py health supercluster
```


```
(venv) crossref/elasticsearch-tools [main●] » python3 src/main.py --help

 Usage: main.py [OPTIONS] COMMAND [ARGS]...

╭─ Options ────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ --install-completion          Install completion for the current shell.                                              │
│ --show-completion             Show completion for the current shell, to copy it or customize the installation.       │
│ --help                        Show this message and exit.                                                            │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
╭─ Commands ───────────────────────────────────────────────────────────────────────────────────────────────────────────╮
│ allocation                             Return the number of shards allocated to each data node and their disk space. │
│ cancel-task                            Cancel a task.                                                                │
│ change-replicas                        Change the number of replicas on an index.                                    │
│ check-reindex-status                   Return information about the reindex tasks currently executing in the cluster.│
│ create-index                           Create a new index.                                                           │
│ create-index-with-mappings             Create a new index.                                                           │
│ create-snapshot                        Create a snapshot of the specified indices.                                   │
│ create-split                           Split an existing index into a new index with more primary shards.            │
│ health                                 Return the health status of a cluster.                                        │
│ index-mappings                         Show the mappings or fields for an index.                                     │
│ index-settings                         Get the index settings.                                                       │
│ indices                                Return high-level information about indices in a cluster.                     │
│ list-snapshots                         Return information about the snapshots stored in one or more repositories.    │
│ make-index-readonly                    Make an index readonly prior to splitting.                                    │
│ nodes                                  Return cluster nodes information.                                             │
│ recovery-status                        Return snapshot restore status.                                               │
│ register-snapshot-repository-readonly  Register a S3 bucket as a snapshot repository.                                │
│ register-snapshot-repository-readwrite Register a S3 bucket as a snapshot repository.                                │
│ reindex                                Copy index contents from a source to a destination.                           │
│ repositories                           Return the snapshot repositories for a cluster.                               │
│ restore-snapshot                       Restore a snapshot of the specified indices.                                  │
│ segments                               Return low-level information about the Lucene segments in index shards.       │
│ settings                               Return cluster-wide settings.                                                 │
│ shards                                 Return the detailed view of what nodes contain which shards.                  │
│ snapshot-status                        Return the current status of any running snapshots.                           │
│ stats                                  Return cluster statistics.                                                    │
╰──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
```


```
(venv) crossref/elasticsearch-tools [main●] » python3 src/main.py health --help

 Usage: main.py health [OPTIONS] POOL

 Return the health status of a cluster.
 https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html

╭─ Arguments ────────────────────────────────────╮
│ *    pool      TEXT  [default: None] [required]│
╰────────────────────────────────────────────────╯
╭─ Options ──────────────────────────────────────╮
│ --help          Show this message and exit.    │
╰────────────────────────────────────────────────╯
```

## AWS IAM and Security Group rules

On the Elasticsearch/Opensearch cluster you need to add a domain policy which gives the IAM role used with this script 
the permissions to access the cluster. For example:

```
  statement {
    actions = [
      "es:ESHttpPut",
      "es:ESHttpPost",
      "es:ESHttpPatch",
      "es:ESHttpHead",
      "es:ESHttpGet",
      "es:ESHttpDelete",
    ]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::$account_id:role/$role_name"]
    }

    resources = [
      "$cluster_arn/*"
    ]
  }
```

The security group for the Elasticsearch/Opensearch cluster also needs a rule adding to allow HTTPS on port 443 from the 
script run location to the cluster. For example:

```
resource "aws_security_group_rule" "opensearch_https_ingress" {
  security_group_id        = aws_security_group.$opensearch_cluster.id
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  description              = "Allow HTTPS to OpenSearch"
  source_security_group_id = aws_security_group.$security_group.id
}
```
