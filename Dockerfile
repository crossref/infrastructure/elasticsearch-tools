FROM public.ecr.aws/docker/library/python:3.11

WORKDIR /app

COPY requirements/production.txt requirements.txt
RUN pip install --no-cache-dir uv && uv pip install --system --no-cache-dir -r requirements.txt

COPY . /app

ENTRYPOINT ["python3", "/app/src/main.py"]
