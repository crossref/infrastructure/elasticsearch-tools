## Order of operations

```
                   ┌─────────────┐                   
          ┌───────▶│  S3 bucket  │──────────┐        
          │        └─────────────┘          │        
   ┌────────────┐                    ┌────────────┐  
   │ 2. Create  │                    │ 4. Restore │  
   │  snapshot  │                    │  snapshot  │  
   └────────────┘                    └────────────┘  
          │                                 │        
          │                                 │        
┌───────────────────┐                       ▼        
│Existing ES Cluster│              ┌────────────────┐
└───────────────────┘              │ New ES Cluster │
          ▲                        └────────────────┘
          │                                 ▲        
    ┌───────────┐                           │        
    │1. Register│                     ┌───────────┐  
    │repository │                     │3. Register│  
    │ readwrite │                     │repository │  
    └───────────┘                     │ readonly  │  
          │                           └───────────┘  
          │       ┌───────────────┐         │        
          └───────│  dbconnector  │─────────┘        
                  └───────────────┘                  
```

In the commands below, "public" refers to the public pool and "other" refers to the new cluster.

- Create the bucket and snapshot role

```
tf plan
tf apply
```

- Register the repository with write access 

```
python3 src/main.py register-snapshot-repository-readwrite public "manual-snapshots" "bucket_name" "us-east-1" "snapshot_role_arn"
```

- List the repositories 

```
python3 src/main.py repositories public
```

- Create a manual snapshot 

```
# Only the work index
python3 src/main.py create-snapshot public "manual-snapshots" "2023-06-22-15-36" "work"

# The work and journal indices
python3 src/main.py create-snapshot public "manual-snapshots" "2023-06-22-15-36" "work,journal"
```

- Check the snapshot status 

```
python3 src/main.py snapshot-status public
```

- List the completed snapshots 

```
python3 src/main.py list-snapshots public "manual-snapshots"
```

- Create the new cluster

```
tf plan
tf apply
```

- Register the repository with readonly access 

```
python3 src/main.py register-snapshot-repository-readonly other "manual-snapshots" "bucket_name" "us-east-1" "snapshot_role_arn"
```

- List the repositories 

```
python3 src/main.py repositories other
```

- Restore the manual snapshot 

```
python3 src/main.py restore-snapshot other "manual-snapshots" "2023-06-22-15-36" "work"
```

- Check the recovery status 

```
python3 src/main.py recovery-status other
```

- Check the indices status 

```
python3 src/main.py indices other
```

- Check the cluster health 

```
python3 src/main.py health other
```

- Cleanup





Most of the following notes were taken from this page: <https://docs.aws.amazon.com/opensearch-service/latest/developerguide/managedomains-snapshots.html> which should be considered authoritative over anything noted here.

Throughout this document, the word "domain" refers to the Elasticsearch cluster and "domain endpoint" is the URL which is used to send API requests to the Elasticsearch cluster.


- Automated snapshots are only for cluster recovery. These are stored in an AWS managed bucket free of charge.
- Manual snapshots are for cluster recovery or for moving data from one cluster to another. These require you to manage a bucket and normal S3 charges apply.


## Prerequisites

- S3 bucket
- Register the repository - This is to tell elasticsearch how to send the snapshot data to the specified bucket
- Fine-grained access control is not enabled so skip step 1
- An IAM role to allow the opensearch service to put the snapshot into the bucket


## Registering the snapshot repository
Registering a snapshot repository is a one-time operation. However, to migrate from one domain to another, you have to register the same snapshot repository on the old domain and the new domain. The repository name is arbitrary.

- When registering the repository on the new domain, add `"readonly": true` to the `"settings"` block of the PUT request. This setting prevents you from accidentally overwriting data from the old domain. Only one domain should have write access to the repository.
- Repository names cannot start with "cs-". Additionally, you shouldn't write to the same repository from multiple domains. Only one domain should have write access to the repository.
- S3 bucket and opensearch domain should be in the same region

Existing domain 

```
PUT domain-endpoint/_snapshot/my-snapshot-repo-name
{
  "type": "s3",
  "settings": {
    "bucket": "s3-bucket-name",
    "region": "region",
    "role_arn": "arn:aws:iam::123456789012:role/TheSnapshotRole"
  }
}
```

New domain

```
PUT domain-endpoint/_snapshot/my-snapshot-repo-name
{
  "type": "s3",
  "settings": {
    "bucket": "s3-bucket-name",
    "region": "region",
    "role_arn": "arn:aws:iam::123456789012:role/TheSnapshotRole",
    "readonly": true
  }
}
```


### Taking the manual snapshot

Snapshots are not instantaneous. They take time to complete and don't represent perfect point-in-time views of the cluster. While a snapshot is in progress, you can still index documents and make other requests to the cluster, but new documents and updates to existing documents generally aren't included in the snapshot. The snapshot includes primary shards as they existed when OpenSearch initiated the snapshot. Depending on the size of your snapshot thread pool, different shards might be included in the snapshot at slightly different times.

You specify the following information when you create a snapshot:

- The name of your snapshot repository
- A name for the snapshot

The time required to take a snapshot increases with the size of the OpenSearch Service domain. Long-running snapshot operations sometimes encounter the following error: `504 GATEWAY_TIMEOUT`. You can typically ignore these errors and wait for the operation to complete successfully. Run the following command to verify the state of all snapshots of your domain:
`curl -XGET 'domain-endpoint/_snapshot/repository-name/_all?pretty'`

You can't take a snapshot if one is currently in progress. To check, run the following command:
`curl -XGET 'domain-endpoint/_snapshot/_status'`

Opensearch docs: <https://opensearch.org/docs/1.1/opensearch/snapshot-restore/#take-snapshots>

### Restoring the manual snapshot

You can't restore a snapshot of your indexes to an OpenSearch cluster that already contains indexes with the same names.

- Delete the indexes on the existing OpenSearch Service domain and then restore the snapshot.
- Should prefer a blank cluster.
