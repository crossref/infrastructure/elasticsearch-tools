In order to send requests to the Elasticsearch API you need two things:

- Security group (firewall) access
- IAM role with permissions

The security group needs to allow TCP 443 to the Elasticsearch cluster.

The Elasticsearch domain policy should permit the IAM roles to read or write to the API.

For example, the following terraform code would add a policy to allow any roles in the `var.read_only_iam_role_arns` list to have read only access and any roles in the `var.read_write_iam_role_arns` to have read/write access.


```hcl
resource "aws_elasticsearch_domain_policy" "elastic_search" {
  domain_name = aws_elasticsearch_domain.elastic_search.domain_name

  access_policies = data.aws_iam_policy_document.elastic_search.json
}

data "aws_iam_policy_document" "elastic_search" {
  statement {
    actions = [
      "es:ESHttpGet",
      "es:ESHttpHead"
    ]

    principals {
      type        = "AWS"
      identifiers = var.read_only_iam_role_arns
    }

    resources = [
      "${aws_elasticsearch_domain.elastic_search.arn}/*"
    ]
  }

  statement {
    actions = [
      "es:ESHttpGet",
      "es:ESHttpHead",
      "es:ESHttpPatch",
      "es:ESHttpPost",
      "es:ESHttpPut",
    ]

    principals {
      type        = "AWS"
      identifiers = var.read_write_iam_role_arns
    }

    resources = [
      "${aws_elasticsearch_domain.elastic_search.arn}/*"
    ]
  }
}
```
