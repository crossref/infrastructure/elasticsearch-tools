# Solr

Works in a similar way to Elasticsearch in that there is a cluster/pool across multiple servers and indices storing data as shards. We are using the older replication method instead of sharding across nodes so there is a primary node and multiple replicas. The primary is read/write and the replicas are read only.

You interact with the cluster via an HTTP API. The `src/solr.py` script will handle constructing the correct URL with host and paths.

## Backup

The HTTP action to create a backup is asynchronous so it will return a response instantly but you then follow up with the status command to check progress.

The backup will create files by default in the data directory in a new folder called `snapshot.$timestamp` or you can provide a location to put the files elsewhere.

Its per index as well so if you have multiple indices you need to run the commands multiple times.

In the output of the backup-status command, one of the first items in the XML response output is `indexSize` which tells you in GB how much space the index and therefore the backup will use. 

Make sure to check that you have sufficient free space on the drive for the given location.

You can run the backup on a replica but it might not have the most recent data if there is replication lag.

```
# Create a backup
python3 src/solr.py create-backup solrprod index "/tmp"

# Check backup status
python3 src/solr.py backup-status solrprod index 
```

The files can then be rsync'ed to other places.

## Restore

Prior to restoring the data, you need to connect to the web interface on the new solr server. Go to Core Admin then add a core for each index to be restored.

```
name: index
instanceDir: index
dataDir: data
config: conf/solrconfig.xml
schema: conf/schema.xml
```

To restore the data, run the restore-backup command. The backup name is the timestamp part of the backup folder name.

```
# Restore the backup
python3 src/solr.py restore-backup newsolr index "/tmp/index.backup" "$timestamp"

# Check progress
python3 src/solr.py restore-status newsolr index
```

If that command fails, check the folder permissions in the data directory to ensure it can write the backup data there.
