Follow the steps in snapshot.md first to create and restore a snapshot. It should be renamed to `restore-$index`.

Before running the reindex operation, the new index needs to exist already with the correct settings like number of shards and also the mappings (which seems to be like the database schema).

You can get the current mappings from the restored index:

```
python3 src/main.py index-mappings other restore-work
```

Write the output to a file, remove the index name and mappings keys, and reformat as JSON with jq. 

Create the new index with the shards, replicas and mappings:

```
# 12 primaries, 1 replica
python3 src/main.py create-index-with-mappings other work 12 1 "./src/cayenne_mappings.json"
```

Run the reindex operation to copy everything from old to new:

```
python3 src/main.py reindex other restore-work work
```

The reindex will take about 10 hours. The output of the reindex command is a task ID. You can track the status by running:

```
python3 src/main.py check-reindex-status other "xgur4J3kRUu6kxesBWIr4g:1051651"
```

If something goes wrong or its taking too long and you need to stop the reindex, it can be cancelled:

```
python3 src/main.py cancel-task other "xgur4J3kRUu6kxesBWIr4g:1051651"
```
