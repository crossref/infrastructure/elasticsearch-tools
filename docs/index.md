# Elasticsearch Tools

At Crossref, we have multiple Elasticsearch clusters which are named "pools". These correspond to certain service levels and rate limits which we provide our customers.

A pool is equivalent to an Elasticsearch domain or cluster.
