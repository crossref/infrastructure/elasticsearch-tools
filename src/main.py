"""A collection of scripts for interacting with Elasticsearch clusters."""

import json
import os
import sys
from pathlib import Path

import boto3
import requests
import typer
from typing_extensions import Annotated
from dotenv import load_dotenv
from requests_aws4auth import AWS4Auth

app = typer.Typer()
load_dotenv()


def authenticate_aws() -> AWS4Auth:
    """Authenticate to AWS and return the tokens."""
    region = "us-east-1"
    service = "es"

    credentials = boto3.Session().get_credentials()
    return AWS4Auth(
        credentials.access_key,
        credentials.secret_key,
        region,
        service,
        session_token=credentials.token,
    )


def get_pool(pool: str) -> str:
    """Translate pool string into domain endpoint.

    Converts pool string to uppercase environment variable key and returns
    the value. Used with the dotenv file.
    """
    host = os.getenv(f"{pool.upper()}")
    if host is None:
      print(f"Pool not found: {pool}")
      sys.exit(1)

    return host


def get_metrics(host: str, path: str) -> str:
    """Send request to the ES API for the given query."""
    awsauth = authenticate_aws()
    url = host + path
    r = requests.get(url, auth=awsauth, timeout=10)
    print(r.text)
    return r.text


@app.command()
def health(pool: str) -> str:
    """Return the health status of a cluster.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html
    """
    host = get_pool(pool)
    path = "_cluster/health?pretty"
    return get_metrics(host, path)


@app.command()
def stats(pool: str) -> str:
    """Return cluster statistics.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-stats.html
    """
    host = get_pool(pool)
    path = "_stats?pretty"
    return get_metrics(host, path)


@app.command()
def nodes(pool: str) -> str:
    """Return cluster nodes information.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-nodes-info.html
    """
    host = get_pool(pool)
    path = "_nodes?pretty"
    return get_metrics(host, path)


@app.command()
def shards(pool: str) -> str:
    """Return the detailed view of what nodes contain which shards.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-shards.html
    """
    host = get_pool(pool)
    path = "_cat/shards?v"
    return get_metrics(host, path)


@app.command()
def indices(pool: str, json: Annotated[bool, typer.Option()] = False) -> str:
    """Return high-level information about indices in a cluster.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-indices.html
    """
    host = get_pool(pool)
    if json:
        path = "_cat/indices?format=json"
    else:
        path = "_cat/indices?v"
    return get_metrics(host, path)


@app.command()
def settings(pool: str) -> str:
    """Return cluster-wide settings.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-get-settings.html
    """
    host = get_pool(pool)
    path = "_cluster/settings?pretty"
    return get_metrics(host, path)


@app.command()
def allocation(pool: str) -> str:
    """Return the number of shards allocated to each data node and their disk space.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-allocation.html
    """
    host = get_pool(pool)
    path = "_cat/allocation?v"
    return get_metrics(host, path)


@app.command()
def segments(pool: str) -> str:
    """Return low-level information about the Lucene segments in index shards.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-segments.html
    """
    host = get_pool(pool)
    path = "_cat/segments/work?v"
    return get_metrics(host, path)


@app.command()
def repositories(pool: str) -> str:
    """Return the snapshot repositories for a cluster.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-repositories.html
    """
    host = get_pool(pool)
    path = "_cat/repositories?v"
    return get_metrics(host, path)


@app.command()
def recovery_status(pool: str) -> str:
    """Return snapshot restore status.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-recovery.html
    """
    host = get_pool(pool)
    path = "_cat/recovery?v"
    return get_metrics(host, path)


@app.command()
def list_snapshots(pool: str, repository_name: str) -> str:
    """Return information about the snapshots stored in one or more repositories.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/cat-snapshots.html
    """
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_cat/snapshots/{repository_name}?v"
    url = host + path
    r = requests.get(url, auth=awsauth, timeout=10)
    print(r.text)
    return r.text


@app.command()
def register_snapshot_repository_readwrite(
    pool: str,
    repository_name: str,
    bucket: str,
    region: str,
    role_arn: str,
) -> str:
    """Register a S3 bucket as a snapshot repository.

    Only one Elasticsearch cluster/domain should have write access. If more than one has
    write access, they could potentially overwrite each others snapshots. If other
    clusters require access to the same repository they can be set as read only.

    The role ARN should be in the format arn:aws:iam::123456789012:role/snapshot-role.
    """
    if repository_name.startswith("cs-"):
        print("Repository names cant start with cs- since that is reserved by AWS")
        sys.exit(1)

    payload = {
        "type": "s3",
        "settings": {"bucket": bucket, "region": region, "role_arn": role_arn},
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_snapshot/{repository_name}"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def register_snapshot_repository_readonly(
    pool: str,
    repository_name: str,
    bucket: str,
    region: str,
    role_arn: str,
) -> str:
    """Register a S3 bucket as a snapshot repository.

    Only one Elasticsearch cluster/domain should have write access. If more than one has
    write access, they could potentially overwrite each others snapshots. If other
    clusters require access to the same repository they can be set as read only.

    The role ARN should be in the format arn:aws:iam::123456789012:role/snapshot-role.
    """
    if repository_name.startswith("cs-"):
        print("Repository names cant start with cs- since that is reserved by AWS")
        sys.exit(1)

    payload = {
        "type": "s3",
        "settings": {
            "bucket": bucket,
            "region": region,
            "role_arn": role_arn,
            "readonly": "true",
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_snapshot/{repository_name}"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def create_snapshot(
    pool: str,
    repository_name: str,
    snapshot_name: str,
    indices: str,
) -> str:
    """Create a snapshot of the specified indices.

    Indices is a comma separated string of the indices like "work" or
    "work,journal". Can be one or more.

    The first snapshot in a repository will be full and may take a long
    time. After that, snapshots are incremental and will complete faster.
    """
    payload = {
        "indices": indices,
        "ignore_unavailable": "true",
        "include_global_state": "false",
        "partial": "false",
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_snapshot/{repository_name}/{snapshot_name}"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def restore_snapshot(
    pool: str,
    repository_name: str,
    snapshot_name: str,
    indices: str,
) -> str:
    """Restore a snapshot of the specified indices.

    Indices is a comma separated string of the indices like "work" or
    "work,journal". Can be one or more.

    You can't restore a snapshot of your indexes to a cluster that already contains
    indexes with the same names. The restored index will be renamed to `restored-$index`.
    """
    payload = {
        "indices": indices,
        "rename_pattern": "(.+)",
        "rename_replacement": "restored-$1"
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_snapshot/{repository_name}/{snapshot_name}/_restore"
    url = host + path
    r = requests.post(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def snapshot_status(pool: str) -> str:
    """Return the current status of any running snapshots."""
    host = get_pool(pool)
    path = "_snapshot/_status"
    return get_metrics(host, path)


@app.command()
def create_index(pool: str, index: str, shards: int, replicas: int) -> str:
    """Create a new index.

    This creates an index without any schema/mappings. Use create_index_with_mappings instead.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
    """
    payload = {
        "settings": {
            "number_of_shards": shards,
            "number_of_replicas": replicas,
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{index}"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def reindex(pool: str, old_index_name: str, new_index_name: str) -> str:
    """Copy index contents from a source to a destination.

    The new index should already exist and have the shards/replicas set up.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html
    """
    payload = {
        "source": {
            "index": old_index_name,
        },
        "dest": {
            "index": new_index_name,
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = "_reindex?wait_for_completion=false"
    url = host + path
    r = requests.post(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def check_reindex_status(pool: str, task_id: str) -> str:
    """Return information about the reindex tasks currently executing in the cluster.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/tasks.html
    """
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_tasks/{task_id}"
    url = host + path
    r = requests.get(url, auth=awsauth, timeout=10)
    print(r.text)
    return r.text


@app.command()
def make_index_readonly(pool: str, index_name: str) -> str:
    """Make an index readonly prior to splitting.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-split-index.html#split-index-api-prereqs
    """
    payload = {
        "settings": {
            "index.blocks.write": "true",
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{index_name}/_settings"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def create_split(
    pool: str, old_index_name: str, new_index_name: str, number_of_shards: int,
) -> str:
    """Split an existing index into a new index with more primary shards.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-split-index.html
    """
    payload = {
        "settings": {
            "index.number_of_shards": number_of_shards,
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{old_index_name}/_split/{new_index_name}"
    url = host + path
    r = requests.post(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text


@app.command()
def change_replicas(pool: str, index: str, replicas: int) -> str:
    """Change the number of replicas on an index.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-update-settings.html
    """
    payload = {
        "index": {
            "number_of_replicas": replicas,
        },
    }
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{index}/_settings"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text

@app.command()
def index_settings(pool: str, index: str) -> str:
    """Get the index settings.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-get-settings.html
    """
    host = get_pool(pool)
    path = "{index}/_settings?pretty"
    return get_metrics(host, path)

@app.command()
def index_mappings(pool: str, index: str) -> str:
    """Show the mappings or fields for an index.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html
    """
    host = get_pool(pool)
    path = f"{index}/_mapping?pretty"
    return get_metrics(host, path)


@app.command()
def create_index_with_mappings(pool: str, index: str, shards: int, replicas: int, mappings_file: Path) -> str:
    """Create a new index.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html#mappings
    """
    settings = {
        "number_of_shards": shards,
        "number_of_replicas": replicas,
    }

    with Path.open(mappings_file) as mappings_json:
        mappings = json.load(mappings_json)

    payload = {}
    payload["settings"] = settings
    payload["mappings"] = mappings
    headers = {"Content-Type": "application/json"}
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{index}"
    url = host + path
    r = requests.put(url, auth=awsauth, json=payload, headers=headers, timeout=10)
    print(r.text)
    return r.text

@app.command()
def delete_index(pool: str, index: str) -> str:
    """Delete an index

    https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-delete-index.html
    """
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"{index}"
    url = host + path
    r = requests.delete(url, auth=awsauth, timeout=10)
    print(r.text)
    return r.text

@app.command()
def cancel_task(pool: str, task_id: str) -> str:
    """Cancel a task.

    https://www.elastic.co/guide/en/elasticsearch/reference/current/tasks.html#task-cancellation
    """
    awsauth = authenticate_aws()
    host = get_pool(pool)
    path = f"_tasks/{task_id}/_cancel"
    url = host + path
    r = requests.post(url, auth=awsauth, timeout=10)
    print(r.text)
    return r.text

if __name__ == "__main__":
    app()
