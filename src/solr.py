"""A collection of scripts for interacting with Solr clusters."""

import os
import sys

import requests
import typer
from dotenv import load_dotenv

app = typer.Typer()
load_dotenv()


def get_pool(pool: str) -> str:
    """Translate pool string into domain endpoint.

    Converts pool string to uppercase environment variable key and returns
    the value. Used with the dotenv file.
    """
    host = os.getenv(f"{pool.upper()}")
    if host is None:
        print(f"Pool not found: {pool}")
        sys.exit(1)

    return host


def get_metrics(host: str, path: str) -> str:
    """Send request to the Solr API for the given query."""
    url = host + path
    r = requests.get(url, timeout=10)
    print(r.text)
    return r.text


@app.command()
def create_backup(pool: str, index: str, location: str) -> str:
    """Create a backup of the Solr index.

    A directory is created with a name like `/$location/snapshot.$timestamp`.

    https://solr.apache.org/guide/6_6/making-and-restoring-backups.html#backup-api
    """
    host = get_pool(pool)
    path = f"solr/{index}/replication?command=backup&location={location}"
    return get_metrics(host, path)


@app.command()
def backup_status(pool: str, index: str) -> str:
    """Check the status of a backup.

    https://solr.apache.org/guide/6_6/making-and-restoring-backups.html#backup-status
    """
    host = get_pool(pool)
    path = f"solr/{index}/replication?command=details"
    return get_metrics(host, path)


@app.command()
def restore_backup(pool: str, index: str, location: str, name: str) -> str:
    """Restore an index from the given backup.

    The name is the timestamp part of the backup directory name.

    https://solr.apache.org/guide/6_6/making-and-restoring-backups.html#restore-api
    """
    host = get_pool(pool)
    path = f"solr/{index}/replication?command=restore&location={location}&name={name}"
    return get_metrics(host, path)


@app.command()
def restore_status(pool: str, index: str) -> str:
    """Check the status of a restore.

    https://solr.apache.org/guide/6_6/making-and-restoring-backups.html#restore-status-api
    """
    host = get_pool(pool)
    path = f"solr/{index}/replication?command=restorestatus"
    return get_metrics(host, path)


if __name__ == "__main__":
    app()
