from locust import HttpUser, task

# 100 rps
# locust --headless --users 2000 --spawn-rate 100 -H https://cayenne-api.staging.crossref.org
#    @task(100)
#    def doi(self):
#        self.client.get("/works/10.1007/BF02744936")
#        self.client.get("/works/10.1016/j.mric.2015.05.013")
#        self.client.get("/works/10.1097/FPC.0b013e3283482502")
#        self.client.get("/works/10.1186/s40851-019-0130-6")
#        self.client.get("/works/10.1017/S1751731118001489")
#        self.client.get("/works/10.1186/s40168-018-0492-6")
#        self.client.get("/works/10.1038/s41571-020-0350-x")
#        self.client.get("/works/10.1007/BF02744936")
# spawn rate of 100 is the max
# users can be increased to increase the rps but it needs more CPU
# 
# 220 rps
# locust --headless --users 2000 --spawn-rate 100 -H https://cayenne-api.staging.crossref.org
#
#    @task(10)
#    def doi1(self):
#        self.client.get("/works/10.1007/BF02744936")
#        self.client.get("/works/10.1016/j.mric.2015.05.013")
#        self.client.get("/works/10.1097/FPC.0b013e3283482502")
#        self.client.get("/works/10.1186/s40851-019-0130-6")
#        self.client.get("/works/10.1017/S1751731118001489")
#        self.client.get("/works/10.1186/s40168-018-0492-6")
#        self.client.get("/works/10.1038/s41571-020-0350-x")
#
#    @task(20)
#    def doi2(self):
#        self.client.get("/works/10.1109/iedm.1970.188219")
#        self.client.get("/works/10.1002/ana.10535")
#        self.client.get("/works/10.1007/s00163-016-0221-8")
#        self.client.get("/works/10.1016/j.vetmic.2011.10.024")
#        self.client.get("/works/10.1288/00005537-199212000-00027")
#        self.client.get("/works/10.1016/0167-739x%2889%2990004-6")
#        self.client.get("/works/10.1016/s0005-2728(00)00285-1")
#
#    @task(30)
#    def doi3(self):
#        self.client.get("/works/10.4014/jmb.1501.01039")
#        self.client.get("/works/10.1177/001088047801800410")
#        self.client.get("/works/10.3390/ijms21197089")
#        self.client.get("/works/10.1007/s11912-018-0706-x")
#        self.client.get("/works/10.1038/s41467-020-20785-x")
#        self.client.get("/works/10.1007/BF02744936")

class CayenneUser(HttpUser):
    @task(10)
    def doi1(self):
        self.client.get("/works/10.1007/BF02744936")
        self.client.get("/works/10.1016/j.mric.2015.05.013")
        self.client.get("/works/10.1097/FPC.0b013e3283482502")
        self.client.get("/works/10.1186/s40851-019-0130-6")
        self.client.get("/works/10.1017/S1751731118001489")
        self.client.get("/works/10.1186/s40168-018-0492-6")
        self.client.get("/works/10.1038/s41571-020-0350-x")

    @task(20)
    def doi2(self):
        self.client.get("/works/10.1109/iedm.1970.188219")
        self.client.get("/works/10.1002/ana.10535")
        self.client.get("/works/10.1007/s00163-016-0221-8")
        self.client.get("/works/10.1016/j.vetmic.2011.10.024")
        self.client.get("/works/10.1288/00005537-199212000-00027")
        self.client.get("/works/10.1016/0167-739x%2889%2990004-6")
        self.client.get("/works/10.1016/s0005-2728(00)00285-1")

    @task(30)
    def doi3(self):
        self.client.get("/works/10.4014/jmb.1501.01039")
        self.client.get("/works/10.1177/001088047801800410")
        self.client.get("/works/10.3390/ijms21197089")
        self.client.get("/works/10.1007/s11912-018-0706-x")
        self.client.get("/works/10.1038/s41467-020-20785-x")
        self.client.get("/works/10.1007/BF02744936")
    #@task
    #def works(self):
    #    self.client.get("/works")

    #@task(100)
    #def doi(self):
    #    self.client.get("/works/10.1007/BF02744936")
    #    self.client.get("/works/10.1016/j.mric.2015.05.013")
    #    self.client.get("/works/10.1097/FPC.0b013e3283482502")
    #    self.client.get("/works/10.1186/s40851-019-0130-6")
    #    self.client.get("/works/10.1017/S1751731118001489")
    #    self.client.get("/works/10.1186/s40168-018-0492-6")
    #    self.client.get("/works/10.1038/s41571-020-0350-x")
    #    self.client.get("/works/10.1007/BF02744936")
    #    self.client.get("/works/10.1016/0167-739x%2889%2990004-6")
    #    self.client.get("/works/10.1016/s0005-2728(00)00285-1")
    #    self.client.get("/works/10.1109/iedm.1970.188219")
    #    self.client.get("/works/10.1002/ana.10535")
    #    self.client.get("/works/10.1007/s00163-016-0221-8")
    #    self.client.get("/works/10.1016/j.vetmic.2011.10.024")
    #    self.client.get("/works/10.1288/00005537-199212000-00027")
    #    self.client.get("/works/10.4014/jmb.1501.01039")
    #    self.client.get("/works/10.1177/001088047801800410")
    #    self.client.get("/works/10.3390/ijms21197089")
    #    self.client.get("/works/10.1007/s11912-018-0706-x")
    #    self.client.get("/works/10.1038/s41467-020-20785-x")

    #@task
    #def publisher_name(self):
    #    self.client.get("/works?facet=publisher-name:11")

    #@task
    #def alternative_id(self):
    #    self.client.get("/works?filter=alternative-id:BF00243844")

    #@task
    #def article_number(self):
    #    self.client.get("/works?filter=article-number:20")

    #@task
    #def assertion_group(self):
    #    self.client.get("/works?filter=assertion-group:rights")

    #@task
    #def assertion_peer_reviewed(self):
    #    self.client.get("/works?filter=assertion:peer_reviewed")

    #@task
    #def award_funder(self):
    #    self.client.get("/works?filter=award.funder:100006435")

    #@task
    #def award_number(self):
    #    self.client.get("/works?filter=award.number:1431063")

    #@task
    #def content_domain(self):
    #    self.client.get("/works?filter=content-domain:asm.org")

    #@task
    #def doi_rows_1000(self):
    #    self.client.get("/works?filter=doi%3A10.1096%2Ffj.201800359R&rows=1000")

    #@task
    #def from_created_date(self):
    #    self.client.get("/works?filter=from-created-date%3A2018-07-24&rows=400")

    #@task
    #def between_pub_dates(self):
    #    self.client.get("/works?filter=from-pub-date:2015,until-pub-date:2016")

        #self.client.get("/works")
        #self.client.get("/works?facet=publisher-name:11")
        #self.client.get("/works?filter=alternative-id:BF00243844")
        #self.client.get("/works?filter=article-number:20")
        #self.client.get("/works?filter=assertion-group:rights")
        #self.client.get("/works?filter=assertion:peer_reviewed")
        #self.client.get("/works?filter=award.funder:100006435")
        #self.client.get("/works?filter=award.number:1431063")
        #self.client.get("/works?filter=content-domain:asm.org")
        #self.client.get("/works?filter=doi%3A10.1096%2Ffj.201800359R&rows=1000")
        #self.client.get("/works?filter=doi:10.7717/peerj.5347")
        #self.client.get("/works?filter=from-created-date%3A2018-07-24&rows=400")
        #self.client.get("/works?filter=from-pub-date:2015,until-pub-date:2016")
        #self.client.get("/works?filter=from-update-date%3A2018-07-18%2Cuntil-update-date%3A2018-07-19%2Cuntil-pub-date%3A2018-07-19&offset=0&rows=0")
        #self.client.get("/works?filter=funder:10.13039/100010182,funder:10.13039/100009224,funder:10.13039/100010458,funder:10.13039/100006393,funder:10.13039/100009924,funder:10.13039/100006394,funder:10.13039/100000181,funder:10.13039/100006602,funder:10.13039/100009926,funder:10.13039/100009927,funder:10.13039/100010184,funder:10.13039/100007485,funder:10.13039/100012414,funder:10.13039/100009919,funder:10.13039/100006754,funder:10.13039/100000183,funder:10.13039/100010216,funder:10.13039/100010185,funder:10.13039/100010181,funder:10.13039/100010186,funder:10.13039/100010187,funder:10.13039/100010211,funder:10.13039/100010452,funder:10.13039/100010453,funder:10.13039/100010454,funder:10.13039/100010212,funder:10.13039/100010213,funder:10.13039/100010214,funder:10.13039/100009904,funder:10.13039/100010197,funder:10.13039/100010188,funder:10.13039/100000090,funder:10.13039/100009897,funder:10.13039/100000185,funder:10.13039/100009900,funder:10.13039/100009243,funder:10.13039/100009898,funder:10.13039/100010210,funder:10.13039/1")
        #self.client.get("/works?filter=has-affiliation:true,from-pub-date:2017-01-01,until-pub-date:2017-06-30&query.affiliation=JSS%20University&select=author,title,published-online,is-referenced-by-count,container-title,volume,issue,page,ISSN,ISBN,URL,link,DOI,type&rows=1000")
        #self.client.get("/works?filter=has-assertion:true,from-pub-date:2018-07-16,until-pub-date:2018-08-16")
        #self.client.get("/works?filter=has-domain-restriction:true,license.url:http://creativecommons.org/licenses/by/4.0/")
        #self.client.get("/works?filter=has-full-text%3Atrue")
        #self.client.get("/works?filter=has-license:true,has-full-text:true&query=%3Ci%3EHelicobacter+pylori%3C%2Fi%3E+eradication+in+the+treatment+of+gastric+hyperplastic+polyps%3A+beyond+National+Health+Insurance&rows=10")
        #self.client.get("/works?filter=has-license:true,has-full-text:true&query=&rows=10")
        #self.client.get("/works?filter=has-orcid:true,from-pub-date:2004-04-04,until-pub-date:2005-04-04")
        #self.client.get("/works?filter=isbn:9789535117223")
        #self.client.get("/works?filter=issn:0013-838X&sort=issued&order=desc&offset=0")
        #self.client.get("/works?filter=issn:0013-838X,issn:1744-4217&sort=issued&order=desc&offset=0")
        #self.client.get("/works?filter=issn:0013-8738&sort=issued&order=desc&offset=0")
        #self.client.get("/works?filter=issn:0014-2336,issn:1573-5060&sort=issued&order=desc&offset=0")
        #self.client.get("/works?filter=license.url:http://creativecommons.org/licenses/by/4.0/&cursor=*")
        #self.client.get("/works?filter=member:98")
        #self.client.get("/works?filter=orcid:0000-0002-4465-7034")
        #self.client.get("/works?filter=orcid:0000-0003-1419-2405")
        #self.client.get("/works?filter=relation.object:http://dx.doi.org/10.5281/zenodo.1042860")
        #self.client.get("/works?filter=relation.type:has-preprint")
        #self.client.get("/works?filter=type%3Ajournal-article&query=retraction%2Bretracted&offset=2240")
        #self.client.get("/works?filter=updates:10.1371/journal.ppat.1006321")
        #self.client.get("/works?query.affiliation=george+washington+university&filter=has-clinical-trial-number:true&facet=funder-name:*")
        #self.client.get("/works?query.affiliation=university+california+san+diego&filter=has-license:true,has-archive:true,has-abstract:true,has-clinical-trial-number:true")
        #self.client.get("/works?query.author=brembs")
        #self.client.get("/works?query.author=john+ioannidis&sort=published&order=asc")
        #self.client.get("/works?query.author=richard+feynman")
        #self.client.get("/works?query.bibliographic=brembs")
        #self.client.get("/works?query.bibliographic=hermeneutics&rows=100&order=desc&sort=indexed")
        #self.client.get("/works?query.chair=galasso")
        #self.client.get("/works?query.container-title=Am+J+Hum+Genet&query=Hypergonadotropic+ovarian+failure+associated+with+an+inherited+mutation+of+human+bone+morphogenetic+protein-15+%28BMP15%29+gene&rows=1&query.author=Pasquale")
        #self.client.get("/works?query.container-title=Am+J+Hum+Genet&query=PLINK%3A+a+tool+set+for+whole-genome+association+and+population-based+linkage+analyses&rows=1&query.author=Richard")
        #self.client.get("/works?query=Steroid+21-hydroxylase+deficiency+%28congenital+adrenal+hyperplasia%29&rows=1&query.author=New")
        #self.client.get("/works?query.container-title=astrophysics")
        #self.client.get("/works?query.container-title=plos&filter=has-authenticated-orcid:true")
        #self.client.get("/works?query.container-title=plos&filter=reference-visibility:open,is-update:true,has-content-domain:true&rows=5")
        #self.client.get("/works?query.contributor=Bialasiewicz")
        #self.client.get("/works?query.editor=kevin+paterson")
        #self.client.get("/works?query=LDA&select=publisher,title,references-count,is-referenced-by-count,DOI,abstract,created,author&rows=1000&filter=has-orcid:true")
        #self.client.get("/works?query=LIGO&facet=published:50")
        #self.client.get("/works?query=Natural+language+processing+%28almost%29+from+scratch&query.author=Collobert&rows=1")
        #self.client.get("/works?query=blockchain&filter=license.version:tdm")
        #self.client.get("/works?query=chikungunya&facet=funder-doi:10")
        #self.client.get("/works?query=closed%20access")
        #self.client.get("/works?query=ddos&select=publisher,title,references-count,is-referenced-by-count,DOI,abstract,created,author&rows=1000&filter=has-orcid:true")
        #self.client.get("/works?query=dracunculiasis&facet=container-title:10")
        #self.client.get("/works?query=gravitational%20waves&filter=from-print-pub-date:2017-08-16,until-print-pub-date:2018-08-16")
        #self.client.get("/works?query=html&filter=full-text.type:text%2Fhtml")
        #self.client.get("/works?query=immunohistochemistry&filter=prefix:10.17504")
        #self.client.get("/works?query=machine%20learning&filter=full-text.version:am")
        #self.client.get("/works?query=neuroscience&facet=archive:10")
        #self.client.get("/works?query=neutrino&facet=publisher-name:10")
        #self.client.get("/works?query=open%20access&filter=license.delay:180")
        #self.client.get("/works?query=open%20access&filter=type-name:Dissertation")
        #self.client.get("/works?query=predatory%20publishing&filter=has-affiliation:true")
        #self.client.get("/works?query=social%20media&filter=from-posted-date:2017-08-16,until-posted-date:2018-08-16")
        #self.client.get("/works?query=spear")
        #self.client.get("/works?query=thresher+sharks&filter=has-references:true,has-license:true,has-funder:true,has-full-text:true")
        #self.client.get("/works?query=thresher+sharks&sample=5")
        #self.client.get("/works?query=thresher+sharks&sort=deposited&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=indexed&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=is-referenced-by-count&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=issued&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=published&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=published-online&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=published-print&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=references-count&order=asc")
        #self.client.get("/works?query=thresher+sharks&sort=score&order=desc")
        #self.client.get("/works?query=thresher+sharks&sort=updated&order=desc")
        #self.client.get("/works?query.translator=smith+ely+jelliffe")
        #self.client.get("/works?query=%0A%09%09%09%09%09+%0A%09%09%09%09%09+%0A++++++++++++++++%09")
        #self.client.get("/works?query=%0A++++++++%0A++++++++%0A++++++++%0A++++++++%0A++++")
        #self.client.get("/works?query=%20&rows=5")
        #self.client.get("/works?query=%22spatial%20atomic%20layer%20deposition%22&sort=created&filter=type:journal-article")
        #self.client.get("/works?query=%D0%9B%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5+%D0%B3%D0%BB%D0%B0%D1%83%D0%BA%D0%BE%D0%BC%D1%8B+%D0%BF%D1%80%D0%B5%D0%BF%D0%B0%D1%80%D0%B0%D1%82%D0%B0%D0%BC%D0%B8%2C+%D0%BD%D0%B5+%D1%81%D0%BE%D0%B4%D0%B5%D1%80%D0%B6%D0%B0%D1%89%D0%B8%D0%BC%D0%B8+%D0%BA%D0%BE%D0%BD%D1%81%D0%B5%D1%80%D0%B2%D0%B0%D0%BD%D1%82%D0%BE%D0%B2%2C+%D1%8F%D0%B2%D0%BB%D1%8F%D0%B5%D1%82%D1%81%D1%8F+%D0%B2%D0%B0%D0%B6%D0%BD%D0%BE%D0%B9+%D0%B8+%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%BD%D0%BE%D0%B9+%D1%86%D0%B5%D0%BB%D1%8C%D1%8E+%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B3%D0%BE+%0A++++++++++++++++++++++++%D0%9C%D0%B0%D1%80%D0%B8%D0%BD%D0%B0+%D0%A5%D0%BE%D1%83%D0%BF%D1%81+%D0%B8+%D0%94%D1%8D%D0%B2%D0%B8%D0%B4+%D0%91%D1%80%D0%BE%D0%B4%D0%B2%D1%8D%D0%B9++++++++++++++++++++")
        #self.client.get("/works?query=%E2%80%9Cgraphite%E2%80%9D%2C+http%3A%2F%2Fweb.archive.org%2Fweb%2F20050827075854%2Fhttp%3A%2F%2Fen.wikipedia.org%2Fwiki%2Fgraphite%2C+aug.+20%2C+2005%2C+3+pp.&select=DOI%2Ctitle")
        #self.client.get("/works?query=%E2%80%9Cthe+solar+zinc+route.%E2%80%9D+digital+image.+swiss+federal+institute+of+technology%2C+department+of+mechanical+and+process+engineering%2C+zurich.+accessed%3A+jan.+4%2C+2011.+printed%3A+may+20%2C+2011.+%3Chttp%3A%2F%2Fwww.pre.ethz.ch%2Fresearch%2Fprojects%2Fimgs%2Fsolzinc%E2%80%941.jpg%3E.+p.+1.&select=DOI%2Ctitle")
        #self.client.get("/works?query=%E2%97%96Refs%20/%E2%97%97&rows=1")
        #self.client.get("/works?query=%E4%B8%8D%E5%90%8C%E7%94%9F%E5%A2%83%E7%A6%8F%E5%AF%BF%E8%9E%BA%28+%E7%BD%97%E6%B8%A11%2C%E7%89%9F%E5%B8%8C%E4%B8%9C1%2C%E5%AE%8B%E7%BA%A2%E6%A2%851%2C%E9%A1%BE%E5%85%9A%E6%81%A91%2C%E6%9D%A8%E5%8F%B6%E6%AC%A31%2C%E6%B1%AA%E5%AD%A6%E6%9D%B01%2C%E7%BD%97%E5%BB%BA%E4%BB%811%2C%E8%83%A1%E9%9A%90%E6%98%8C1%2A%2A%2C%E7%AB%A0%E5%AE%B6%E6%81%A92+et+al.")
        #self.client.get("/works?query=&rows=3")
        #self.client.get("/works?query=&rows=40")
        #self.client.get("/works?query=&sort=relevance")
        #self.client.get("/works?query=(a)%20Aoyagi,%20T.;%20Aoyama,%20T.;%20Kojima,%20F.;%20Matsuda,%20N.;%20Maruyama,%20M.;%20Hamada,%20M.;%20Takeuchi,%20T.%20Benastatins%20A%20and%20B,%20New%20Inhibitors%20of%20Glutathione%20S-Transferase,%20Produced%20by%20Streptomyces%20sp.%20MI384-DF12.%20I.%20Taxonomy,%20Production,%20Isolation,%20Physico-Chemical%20Properties%20and%20Biological%20Activities.%20J.%20Antibiot.%201992,%2045,%201385-1390.&rows=1")
        #self.client.get("/works?query=(a)%20Chambers,%20H.%20F.;%20Deleo,%20F.%20R.%20Waves%20of%20Resistance:%20Staphylococcus%20aureus%20in%20the%20Antibiotic%20Era.%20Nat.%20Rev.%20Microbiol.%202009,%207,%20629-641.&rows=1")
        #self.client.get("/works?query=(a)%20Fischbach,%20M.%20A.;%20Walsh,%20C.%20T.%20Antibiotics%20for%20Emerging%20Pathogens.%20Science%202009,%20325,%201089-1093.&rows=1")
        #self.client.get("/works?query=Case&filter=issn:0027-5514")
        #self.client.get("/works?query=amateur+astronomy&filter=type:book")
        #self.client.get("/works?query=archival%20preservation&filter=archive:CLOCKSS")
        #self.client.get("/works?query=cancer&filter=funder:100000865")
        #self.client.get("/works?query=neurological&filter=container-title:Cell")
        #self.client.get("/works?query=phishing&rows=5&offset=5")
        #self.client.get("/works?query=phishing&sort=score&order=desc")
        #self.client.get("/works?rows=0&facet=funder-doi:20")
        #self.client.get("/works?rows=1000&filter=from-created-date:2017-11-06,until-created-date:2017-11-07&cursor=*")
        #self.client.get("/works?rows=1000&filter=from-created-date:2018-05-08,until-created-date:2018-05-09")
        #self.client.get("/works?rows=1000&sort=published")
        #self.client.get("/works?rows=5&facet=published:100")
        #self.client.get("/works?rows=500&filter=type%3Ajournal-article%2Cfrom-created-date%3A2018-06-19%2Cuntil-created-date%3A2018-06-19&sort=created&order=desc&cursor=%2A")
        #self.client.get("/works?sort=created&order=asc")
        #self.client.get("/works?sort=deposited&order=desc&rows=1&filter=type:journal-article")
        #self.client.get("/works?sort=deposited&order=desc&rows=1&filter=type:journal-article,has-full-text:true")
        #self.client.get("/works?sort=indexed")
        #self.client.get("/works?sort=is-referenced-by-count&order=desc")
        #self.client.get("/works?sort=is-referenced-by-count&rows=100&filter=type%3Ajournal-article&query=stone")
        #self.client.get("/works?sort=issued&order=desc")
        #self.client.get("/works?sort=score&order=desc&rows=10&facet=type-name:10,published:10,container-title:10,publisher-name:10,funder-name:10&query.bibliographic=A%20supermassive%20black%20hole%20in%20an%20ultra%20compact%20dwarf%20galaxy&query.funder-name=NIH")
        #self.client.get("/works?sort=score&query=genome%20project&filter=type%3Ajournal-article&rows=20&offset=0")
        #self.client.get("/works?sort=score&query=genome%20project&rows=3&offset=0")
        
#/funders
#/funders/10.13039/100000001/works?rows=0&facet=funder-doi%3A*&filter=member%3A15%2Cmember%3A16%2Cmember%3A78%2Cmember%3A98%2Cmember%3A179%2Cmember%3A221%2Cmember%3A221%2Cmember%3A263%2Cmember%3A286%2Cmember%3A292%2Cmember%3A301%2Cmember%3A311%2Cmember%3A316%2Cmember%3A317%2Cmember%3A320
#/funders/10.13039/100000015/works
#/funders/10.13039/100000015/works?rows=0&facet=funder-doi%3A*&filter=member%3A3%2Cmember%3A12%2Cmember%3A13%2Cmember%3A14%2Cmember%3A15%2Cmember%3A16%2Cmember%3A20%2Cmember%3A33%2Cmember%3A37%2Cmember%3A56%2Cmember%3A78%2Cmember%3A98%2Cmember%3A150%2Cmember%3A175%2Cmember%3A179%2Cmember%3A186%2Cmember%3A187%2Cmember%3A189%2Cmember%3A221%2Cmember%3A224%2Cmember%3A233%2Cmember%3A237%2Cmember%3A263%2Cmember%3A266%2Cmember%3A285%2Cmember%3A286%2Cmember%3A291%2Cmember%3A292%2Cmember%3A297%2Cmember%3A301%2Cmember%3A311%2Cmember%3A316%2Cmember%3A317%2Cmember%3A320%2Cmember%3A341%2Cmember%3A351%2Cmember%3A414%2Cmember%3A859%2Cmember%3A1167%2Cmember%3A1387%2Cmember%3A1394%2Cmember%3A2208%2Cmember%3A4285%2Cmember%3A8935%2Cmember%3A12380%2Cmember%3A49231%2Cmember%3A66637%2Cmember%3A74214%2Cmember%3A82652%2Cmember%3A98240
#/funders/100000005/works?cursor=*&filter=type%3Ajournal-article%2Cfrom-deposit-date%3A2018-08-01%2Cuntil-deposit-date%3A2018-08-08&rows=100
#/funders/100000005/works?cursor=*&filter=type%3Aproceedings-article%2Cfrom-deposit-date%3A2017-08-01%2Cuntil-deposit-date%3A2018-08-08&rows=100
#/funders/100000015/works?rows=20&sort=published&order=desc&filter=member%3A3%2Cmember%3A12%2Cmember%3A13%2Cmember%3A14%2Cmember%3A15%2Cmember%3A16%2Cmember%3A20%2Cmember%3A33%2Cmember%3A37%2Cmember%3A56%2Cmember%3A78%2Cmember%3A98%2Cmember%3A150%2Cmember%3A175%2Cmember%3A179%2Cmember%3A186%2Cmember%3A187%2Cmember%3A189%2Cmember%3A221%2Cmember%3A224%2Cmember%3A233%2Cmember%3A237%2Cmember%3A263%2Cmember%3A266%2Cmember%3A285%2Cmember%3A286%2Cmember%3A291%2Cmember%3A292%2Cmember%3A297%2Cmember%3A301%2Cmember%3A311%2Cmember%3A316%2Cmember%3A317%2Cmember%3A320%2Cmember%3A341%2Cmember%3A351%2Cmember%3A414%2Cmember%3A859%2Cmember%3A1167%2Cmember%3A1387%2Cmember%3A1394%2Cmember%3A2208%2Cmember%3A4285%2Cmember%3A8935%2Cmember%3A12380%2Cmember%3A49231%2Cmember%3A66637%2Cmember%3A74214%2Cmember%3A82652%2Cmember%3A98240
#/funders/100000015/works?filter=type:journal-article&sort=indexed&order=desc
#/funders/100000865/works?facet=category-name:100
#/funders/100000865/works?facet=license:*
#/funders/100000865/works?facet=type-name:*
#/funders/100000865/works?facet=issn:10
#/funders?filter=location:Australia
#/funders?filter=location:United+States
#/funders?filter=location:United+States&query=science
#/funders?filter=location:Canada&query=science
#/funders?query=research+foundation
#/funders?query=science
#/journals
#/journals/1549-7712/works
#/journals/1549-7712/works?facet=orcid:100
#/journals/1549-7712/works?filter=from-created-date:2009-01-20,until-created-date:2017-01-20
#/journals/1741-7015/works?filter=has-funder:true
#/journals/1741-7015/works?query=cancer&rows=4
#/journals/1741-7015/works?cursor=*&filter=type%3Ajournal-article%2Cfrom-deposit-date%3A2017-03-08%2Cuntil-deposit-date%3A2018-08-08&rows=100
#/journals/1680-7375/works?filter=has-relation:true
#/journals/1741-7015/works?filter=has-update-policy:true
#/journals/1741-7015/works?filter=type%3Ajournal-article&rows=5&facet=published%3A*&sort=published&order=desc
#/journals/1941-7020/works?filter=type%3Ajournal-article&rows=5&sort=is-referenced-by-count&order=desc
#/journals?offset=0&rows=250&query=SAGE
#/journals?query=Journal%20of%20the%20American%20Statistical%20Association
#/licenses
#/licenses?query=algorithm+deep
#/licenses?query=creative
#/members
#/members/311/works?facet=issn:10
#/members/311/works?query=neurology
#/members/5003/works?filter=from-deposit-date:2015-01-16,until-deposit-date:2018-08-16
#/members/5003/works?filter=from-index-date:2015-08-16,until-index-date:2020-08-16
#/members/5003/works?filter=from-online-pub-date:2015-08-16,until-online-pub-date:2018-08-16
#/members/78/works?cursor=*&filter=type%3Ajournal-article%2Cfrom-deposit-date%3A2018-08-09%2Cuntil-deposit-date%3A2018-08-08&rows=100
#/members/78/works?facet=license:*
#/members/78/works?filter=from-created-date:2009-01-20,until-created-date:2017-01-20
#/members/78/works?sort=is-referenced-by-count&rows=100&filter=type%3Ajournal-article
#/members/98/works
#/members?filter=backfile-doi-count:3
#/members?filter=current-doi-count:3
#/members?filter=has-public-references:true
#/members?filter=prefix:10.3120
#/members?filter=reference-visibility:open
#/members?query=company&filter=current-doi-count:0
#/members?query=company&filter=reference-visibility:limited
#/members?query=publishing+Company
#/members?query=springer
#/prefixes/10.1016/works?query=ecology&rows=4
#/prefixes/10.1021/works?filter=from-pub-date:1980-01-01,until-pub-date:1980-12-31&rows=1000
#/prefixes/10.1101/works?facet=orcid:100
#/prefixes/10.1101/works?facet=relation-type:*
#/prefixes/10.1101/works?filter=has-relation:true
#/prefixes/10.1126/works?filter=has-funder:true
#/prefixes/10.12688/works?facet=update-type:*
#/prefixes/10.12688/works?filter=has-update-policy:true
#/prefixes/10.6064/works
#/prefixes/10.6064/works?filter=from-created-date:2009-01-20,until-created-date:2017-01-20
#/prefixes/10.7554/works?facet=assertion-group:10
#/prefixes/10.7554/works?facet=assertion:10
#/prefixes/10.7554/works?facet=funder-name:10
#/prefixes/10.7554/works?filter=from-update-date:2018-07-17,until-update-date:2018-08-17
#/prefixes/10.7717/works?facet=affiliation:100
#/types/book-set/works
#/types/journal-article/works?facet=orcid:100
#/types/journal-article/works?filter=category-name%3AGeology
#/types/journal-article/works?filter=has-funder:true
#/types/journal-article/works?query.bibliographic=Abbasi%2C+Z.%2C+%26amp%3B+Peiman%2C+A.+%282012%29.+Brain+death+and+organ+transplantation+in+Iran.+The+Iranian+Journal+of+Medical+Law%2C+6%2820%29%2C+43-54.+
#/types/journal-article/works?query=10.1002
#/types/journal-article/works?query=Global%2bWarming
#/types/journal-article/works?sort=is-referenced-by-count&rows=100&filter=type%3Ajournal-article
#/types/monograph/works?rows=3
#/types/posted-content/works
#/works
#/works?facet=publisher-name:11
#/works?filter=alternative-id:BF00243844
#/works?filter=article-number:20
#/works?filter=assertion-group:rights
#/works?filter=assertion:peer_reviewed
#/works?filter=award.funder:100006435
#/works?filter=award.number:1431063
#/works?filter=content-domain:asm.org
#/works?filter=doi%3A10.1096%2Ffj.201800359R&rows=1000
#/works?filter=doi:10.7717/peerj.5347
#/works?filter=from-created-date%3A2018-07-24&rows=400
#/works?filter=from-pub-date:2015,until-pub-date:2016
#/works?filter=from-update-date%3A2018-07-18%2Cuntil-update-date%3A2018-07-19%2Cuntil-pub-date%3A2018-07-19&offset=0&rows=0
#/works?filter=funder:10.13039/100010182,funder:10.13039/100009224,funder:10.13039/100010458,funder:10.13039/100006393,funder:10.13039/100009924,funder:10.13039/100006394,funder:10.13039/100000181,funder:10.13039/100006602,funder:10.13039/100009926,funder:10.13039/100009927,funder:10.13039/100010184,funder:10.13039/100007485,funder:10.13039/100012414,funder:10.13039/100009919,funder:10.13039/100006754,funder:10.13039/100000183,funder:10.13039/100010216,funder:10.13039/100010185,funder:10.13039/100010181,funder:10.13039/100010186,funder:10.13039/100010187,funder:10.13039/100010211,funder:10.13039/100010452,funder:10.13039/100010453,funder:10.13039/100010454,funder:10.13039/100010212,funder:10.13039/100010213,funder:10.13039/100010214,funder:10.13039/100009904,funder:10.13039/100010197,funder:10.13039/100010188,funder:10.13039/100000090,funder:10.13039/100009897,funder:10.13039/100000185,funder:10.13039/100009900,funder:10.13039/100009243,funder:10.13039/100009898,funder:10.13039/100010210,funder:10.13039/1
#/works?filter=has-affiliation:true,from-pub-date:2017-01-01,until-pub-date:2017-06-30&query.affiliation=JSS%20University&select=author,title,published-online,is-referenced-by-count,container-title,volume,issue,page,ISSN,ISBN,URL,link,DOI,type&rows=1000
#/works?filter=has-assertion:true,from-pub-date:2018-07-16,until-pub-date:2018-08-16
#/works?filter=has-domain-restriction:true,license.url:http://creativecommons.org/licenses/by/4.0/
#/works?filter=has-full-text%3Atrue
#/works?filter=has-license:true,has-full-text:true&query=%3Ci%3EHelicobacter+pylori%3C%2Fi%3E+eradication+in+the+treatment+of+gastric+hyperplastic+polyps%3A+beyond+National+Health+Insurance&rows=10
#/works?filter=has-license:true,has-full-text:true&query=&rows=10
#/works?filter=has-orcid:true,from-pub-date:2004-04-04,until-pub-date:2005-04-04
#/works?filter=isbn:9789535117223
#/works?filter=issn:0013-838X&sort=issued&order=desc&offset=0
#/works?filter=issn:0013-838X,issn:1744-4217&sort=issued&order=desc&offset=0
#/works?filter=issn:0013-8738&sort=issued&order=desc&offset=0
#/works?filter=issn:0014-2336,issn:1573-5060&sort=issued&order=desc&offset=0
#/works?filter=license.url:http://creativecommons.org/licenses/by/4.0/&cursor=*
#/works?filter=member:98
#/works?filter=orcid:0000-0002-4465-7034
#/works?filter=orcid:0000-0003-1419-2405
#/works?filter=relation.object:http://dx.doi.org/10.5281/zenodo.1042860
#/works?filter=relation.type:has-preprint
#/works?filter=type%3Ajournal-article&query=retraction%2Bretracted&offset=2240
#/works?filter=updates:10.1371/journal.ppat.1006321
#/works?query.affiliation=george+washington+university&filter=has-clinical-trial-number:true&facet=funder-name:*
#/works?query.affiliation=university+california+san+diego&filter=has-license:true,has-archive:true,has-abstract:true,has-clinical-trial-number:true
#/works?query.author=brembs
#/works?query.author=john+ioannidis&sort=published&order=asc
#/works?query.author=richard+feynman
#/works?query.bibliographic=brembs
#/works?query.bibliographic=hermeneutics&rows=100&order=desc&sort=indexed
#/works?query.chair=galasso
#/works?query.container-title=Am+J+Hum+Genet&query=Hypergonadotropic+ovarian+failure+associated+with+an+inherited+mutation+of+human+bone+morphogenetic+protein-15+%28BMP15%29+gene&rows=1&query.author=Pasquale
#/works?query.container-title=Am+J+Hum+Genet&query=PLINK%3A+a+tool+set+for+whole-genome+association+and+population-based+linkage+analyses&rows=1&query.author=Richard
#/works?query=Steroid+21-hydroxylase+deficiency+%28congenital+adrenal+hyperplasia%29&rows=1&query.author=New
#/works?query.container-title=astrophysics
#/works?query.container-title=plos&filter=has-authenticated-orcid:true
#/works?query.container-title=plos&filter=reference-visibility:open,is-update:true,has-content-domain:true&rows=5
#/works?query.contributor=Bialasiewicz
#/works?query.editor=kevin+paterson
#/works?query=LDA&select=publisher,title,references-count,is-referenced-by-count,DOI,abstract,created,author&rows=1000&filter=has-orcid:true
#/works?query=LIGO&facet=published:50
#/works?query=Natural+language+processing+%28almost%29+from+scratch&query.author=Collobert&rows=1
#/works?query=blockchain&filter=license.version:tdm
#/works?query=chikungunya&facet=funder-doi:10
#/works?query=closed%20access
#/works?query=ddos&select=publisher,title,references-count,is-referenced-by-count,DOI,abstract,created,author&rows=1000&filter=has-orcid:true
#/works?query=dracunculiasis&facet=container-title:10
#/works?query=gravitational%20waves&filter=from-print-pub-date:2017-08-16,until-print-pub-date:2018-08-16
#/works?query=html&filter=full-text.type:text%2Fhtml
#/works?query=immunohistochemistry&filter=prefix:10.17504
#/works?query=machine%20learning&filter=full-text.version:am
#/works?query=neuroscience&facet=archive:10
#/works?query=neutrino&facet=publisher-name:10
#/works?query=open%20access&filter=license.delay:180
#/works?query=open%20access&filter=type-name:Dissertation
#/works?query=predatory%20publishing&filter=has-affiliation:true
#/works?query=social%20media&filter=from-posted-date:2017-08-16,until-posted-date:2018-08-16
#/works?query=spear
#/works?query=thresher+sharks&filter=has-references:true,has-license:true,has-funder:true,has-full-text:true
#/works?query=thresher+sharks&sample=5
#/works?query=thresher+sharks&sort=deposited&order=desc
#/works?query=thresher+sharks&sort=indexed&order=desc
#/works?query=thresher+sharks&sort=is-referenced-by-count&order=desc
#/works?query=thresher+sharks&sort=issued&order=desc
#/works?query=thresher+sharks&sort=published&order=desc
#/works?query=thresher+sharks&sort=published-online&order=desc
#/works?query=thresher+sharks&sort=published-print&order=desc
#/works?query=thresher+sharks&sort=references-count&order=asc
#/works?query=thresher+sharks&sort=score&order=desc
#/works?query=thresher+sharks&sort=updated&order=desc
#/works?query.translator=smith+ely+jelliffe
#/works?query=%0A%09%09%09%09%09+%0A%09%09%09%09%09+%0A++++++++++++++++%09
#/works?query=%0A++++++++%0A++++++++%0A++++++++%0A++++++++%0A++++
#/works?query=%20&rows=5
#/works?query=%22spatial%20atomic%20layer%20deposition%22&sort=created&filter=type:journal-article
#/works?query=%D0%9B%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5+%D0%B3%D0%BB%D0%B0%D1%83%D0%BA%D0%BE%D0%BC%D1%8B+%D0%BF%D1%80%D0%B5%D0%BF%D0%B0%D1%80%D0%B0%D1%82%D0%B0%D0%BC%D0%B8%2C+%D0%BD%D0%B5+%D1%81%D0%BE%D0%B4%D0%B5%D1%80%D0%B6%D0%B0%D1%89%D0%B8%D0%BC%D0%B8+%D0%BA%D0%BE%D0%BD%D1%81%D0%B5%D1%80%D0%B2%D0%B0%D0%BD%D1%82%D0%BE%D0%B2%2C+%D1%8F%D0%B2%D0%BB%D1%8F%D0%B5%D1%82%D1%81%D1%8F+%D0%B2%D0%B0%D0%B6%D0%BD%D0%BE%D0%B9+%D0%B8+%D1%80%D0%B5%D0%B0%D0%BB%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%BD%D0%BE%D0%B9+%D1%86%D0%B5%D0%BB%D1%8C%D1%8E+%D0%B1%D1%83%D0%B4%D1%83%D1%89%D0%B5%D0%B3%D0%BE+%0A++++++++++++++++++++++++%D0%9C%D0%B0%D1%80%D0%B8%D0%BD%D0%B0+%D0%A5%D0%BE%D1%83%D0%BF%D1%81+%D0%B8+%D0%94%D1%8D%D0%B2%D0%B8%D0%B4+%D0%91%D1%80%D0%BE%D0%B4%D0%B2%D1%8D%D0%B9++++++++++++++++++++
#/works?query=%E2%80%9Cgraphite%E2%80%9D%2C+http%3A%2F%2Fweb.archive.org%2Fweb%2F20050827075854%2Fhttp%3A%2F%2Fen.wikipedia.org%2Fwiki%2Fgraphite%2C+aug.+20%2C+2005%2C+3+pp.&select=DOI%2Ctitle
#/works?query=%E2%80%9Cthe+solar+zinc+route.%E2%80%9D+digital+image.+swiss+federal+institute+of+technology%2C+department+of+mechanical+and+process+engineering%2C+zurich.+accessed%3A+jan.+4%2C+2011.+printed%3A+may+20%2C+2011.+%3Chttp%3A%2F%2Fwww.pre.ethz.ch%2Fresearch%2Fprojects%2Fimgs%2Fsolzinc%E2%80%941.jpg%3E.+p.+1.&select=DOI%2Ctitle
#/works?query=%E2%97%96Refs%20/%E2%97%97&rows=1
#/works?query=%E4%B8%8D%E5%90%8C%E7%94%9F%E5%A2%83%E7%A6%8F%E5%AF%BF%E8%9E%BA%28+%E7%BD%97%E6%B8%A11%2C%E7%89%9F%E5%B8%8C%E4%B8%9C1%2C%E5%AE%8B%E7%BA%A2%E6%A2%851%2C%E9%A1%BE%E5%85%9A%E6%81%A91%2C%E6%9D%A8%E5%8F%B6%E6%AC%A31%2C%E6%B1%AA%E5%AD%A6%E6%9D%B01%2C%E7%BD%97%E5%BB%BA%E4%BB%811%2C%E8%83%A1%E9%9A%90%E6%98%8C1%2A%2A%2C%E7%AB%A0%E5%AE%B6%E6%81%A92+et+al.
#/works?query=&rows=3
#/works?query=&rows=40
#/works?query=&sort=relevance
#/works?query=(a)%20Aoyagi,%20T.;%20Aoyama,%20T.;%20Kojima,%20F.;%20Matsuda,%20N.;%20Maruyama,%20M.;%20Hamada,%20M.;%20Takeuchi,%20T.%20Benastatins%20A%20and%20B,%20New%20Inhibitors%20of%20Glutathione%20S-Transferase,%20Produced%20by%20Streptomyces%20sp.%20MI384-DF12.%20I.%20Taxonomy,%20Production,%20Isolation,%20Physico-Chemical%20Properties%20and%20Biological%20Activities.%20J.%20Antibiot.%201992,%2045,%201385-1390.&rows=1
#/works?query=(a)%20Chambers,%20H.%20F.;%20Deleo,%20F.%20R.%20Waves%20of%20Resistance:%20Staphylococcus%20aureus%20in%20the%20Antibiotic%20Era.%20Nat.%20Rev.%20Microbiol.%202009,%207,%20629-641.&rows=1
#/works?query=(a)%20Fischbach,%20M.%20A.;%20Walsh,%20C.%20T.%20Antibiotics%20for%20Emerging%20Pathogens.%20Science%202009,%20325,%201089-1093.&rows=1
#/works?query=Case&filter=issn:0027-5514
#/works?query=amateur+astronomy&filter=type:book
#/works?query=archival%20preservation&filter=archive:CLOCKSS
#/works?query=cancer&filter=funder:100000865
#/works?query=neurological&filter=container-title:Cell
#/works?query=phishing&rows=5&offset=5
#/works?query=phishing&sort=score&order=desc
#/works?rows=0&facet=funder-doi:20
#/works?rows=1000&filter=from-created-date:2017-11-06,until-created-date:2017-11-07&cursor=*
#/works?rows=1000&filter=from-created-date:2018-05-08,until-created-date:2018-05-09
#/works?rows=1000&sort=published
#/works?rows=5&facet=published:100
#/works?rows=500&filter=type%3Ajournal-article%2Cfrom-created-date%3A2018-06-19%2Cuntil-created-date%3A2018-06-19&sort=created&order=desc&cursor=%2A
#/works?sort=created&order=asc
#/works?sort=deposited&order=desc&rows=1&filter=type:journal-article
#/works?sort=deposited&order=desc&rows=1&filter=type:journal-article,has-full-text:true
#/works?sort=indexed
#/works?sort=is-referenced-by-count&order=desc
#/works?sort=is-referenced-by-count&rows=100&filter=type%3Ajournal-article&query=stone
#/works?sort=issued&order=desc
#/works?sort=score&order=desc&rows=10&facet=type-name:10,published:10,container-title:10,publisher-name:10,funder-name:10&query.bibliographic=A%20supermassive%20black%20hole%20in%20an%20ultra%20compact%20dwarf%20galaxy&query.funder-name=NIH
#/works?sort=score&query=genome%20project&filter=type%3Ajournal-article&rows=20&offset=0
#/works?sort=score&query=genome%20project&rows=3&offset=0
